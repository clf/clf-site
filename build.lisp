(in-package :cl-user)

;;
;; load and/or install quicklisp in dcooper
;;

(let ((homedir (or (probe-file "/home/dcooper/")
                   (probe-file "/home/dcooper8/"))))
  (load (merge-pathnames "quicklisp/setup.lisp" homedir)))

(pushnew (make-pathname :defaults *load-truename* :name nil :type nil)
	 quicklisp:*local-project-directories* :test #'equalp)

(ql:register-local-projects)

(ql:quickload :zaserve)

;;
;; Following stuff temporary until quicklisp updates its zaserve.
;;

(ql:quickload :clf-site)

(gendl:start-gendl!)

(in-package :gwl-user)

(defun make-site ()

  (let ((output-root (merge-pathnames "output/" (glisp:system-home :clf-site))))

    (format t "~&~%Crawling clf-site and outputting into ~a~%~%" output-root)

    (let ((current *developing?*))
      (setq *developing?* nil)
      (gwl:crawl :url-path "/clf" :output-root output-root)
      (setq *developing?* current))))
(clf-site::initialize!)
(sleep 1)
(make-site)


