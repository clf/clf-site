(in-package :clf-site)


#+nil
(defparameter *site-home*
  (let ((source-pathname (glisp:source-pathname)))
    (probe-file (make-pathname :name nil :type nil
			       :defaults (merge-pathnames "../" source-pathname)))))

;;
;; FLAG -- assumes we are running an image built from source with
;; quicklisp.
;;

(defparameter *site-home* (glisp:system-home :clf-site))

(defun initialize! ()

  (setq *developing?* t)
  
  (publish-shared  'index-page :path "/clf")
  
  (let ((css-directory (namestring (probe-file (merge-pathnames "css/" *site-home*))))
	(image-directory (namestring (probe-file (merge-pathnames "images/" *site-home*)))))
    (with-all-servers (server)
      (publish-directory :prefix "/clf-site-images"
			 :server server
			 :destination image-directory)
      (publish-directory :prefix "/clf-site-css"
			 :server server
			 :destination css-directory))))

(initialize!)

