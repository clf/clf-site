all: build

static:
	rm -rf /tmp/sites/clf-site


build: static
	/home/dcooper/local/sbcl/bin/sbcl --no-userinit --load build.lisp --non-interactive
