

# Foundation Roles & Responsibilities

## Chairperson

 - Prepares agenda for monthly Board meetings and for annual
   Stakeholder/Community meetings.
   
 - Conducts monthly Board meetings and annual meeting. 
 
 - Communicates with Working Committees to resolve issues that arise
   between meetings.
   
 - Reviews and negotiates all contracts. 
 
 - Reviews and approves all non-routine correspondences and
   newsletters.
   
 - Represents Foundation in any legal matters. 
 
 - Reviews Financial information on a monthly basis.
 
 - Works with Treasurer on budget and other financial issues. 
 
 - Approves Invoices and other payments per Foundation Payment Policies. 
 
 - Delegates responsibilities to Working Committees and Board members. 
 
 - Suggests and implements policies and procedures approved by the Board. 
 
 - Conducts periodic inspections of Foundation infrastructure and any
   other facilities and recommends maintenance and improvements. 
   
 - Mediates any disputes involving Working Committees, Board, and
   Stakeholders.
   
 - Assists other Board members with inspection of Working Committees
   performance.
   
## Vice Chair / Treasurer

 - Handles Chairperson's responsibilities in his or her absence. 
 
 - Takes minutes at meetings in Secretary's absence. 
 
 - Reviews Financial information on a monthly basis.
 
 - Reports to the Board regarding financial status on a quarterly
   basis.
   
 - Approves invoices and other payments per Foundation payment
   procedure in Chairperson's absence or unavailability.
   
 - Prepares draft of annual budget for presentation to the Board in
   October and recommends adjustments in membership dues or other
   pricing.
   
 - Investigates and recommends cost saving measures. 
 
 - Conducts periodic inspections of Foundation infrastructure and any
   other facilities and recommends maintenance and improvements. 

 - Mediates any disputes involving the Chairperson. 
 
 
## Secretary

 - Prepares minutes of each meeting.
 
 - Drafts all non-routine correspondence for review by Chairperson and
   distribution by respective Working Committee.
   
 - Conducts periodic inspections of Foundation infrastructure and any
   other facilities and recommends maintenance and improvements. 


## Director A

 - Responsible for Winter (January-February) and Fall
   (September-October) Newsletters. 
   
 - Suggests topics for articles in December and August.
 
 - Writes articles and/or works with other board members, Working
   Committees, and stakeholders to write articles.
   
 - Distributes routine correspondences with help from Working
   Committees.
   
 - Conducts periodic inspections of Foundation infrastructure and any
   other facilities and recommends maintenance and improvements. 


## Director B

 - Responsible for Domain Name registration and maintenance with
   possible help from Working Committees.
 
 - Responsible for Hosting payment and maintenance with possible help
   from Working Committees.
   
 - Conducts periodic inspections of Foundation infrastructure and any
   other facilities and recommends maintenance and improvements. 

