;;;; -*- coding: utf-8 -*-

(asdf:defsystem #:clf-site :description
  "The Gendl® clf-site Subsystem" :author "Genworks International"
  :license "Affero Gnu Public License (http://www.gnu.org/licenses/)"
  :serial t :version "20200304" :depends-on
  (:gendl :cl-markdown #-allegro :cl-html-parse)
  :defsystem-depends-on (:gendl-asdf)
  :components
  ((:file "source/patches") (:file "source/package")
   (:gdl "source/activities-page") (:gdl "source/base-page")
   (:gdl "source/board-page") (:gdl "source/contact-page")
   (:gdl "source/domains-page") (:gdl "source/index-page")
   (:file "source/initialize") (:gdl "source/payment-policy-page")))
